package controller

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/mezigar/geotask/module/courierfacade/service"
)

type CourierController struct {
	courierService service.CourierFacer
}

func NewCourierController(courierService service.CourierFacer) *CourierController {
	return &CourierController{courierService: courierService}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	// установить задержку в 50 миллисекунд
	time.Sleep(50 * time.Millisecond)
	// Получить статус курьера из сервиса courierService используя метод GetStatus
	status := c.courierService.GetStatus(ctx)

	// Отправить статус курьера в ответ
	ctx.JSON(http.StatusOK, status)

}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	var cm CourierMove
	// получить данные из m.Data и десериализовать их в структуру CourierMove
	err := json.Unmarshal(m.Data.([]byte), &cm)
	if err != nil {
		fmt.Println("can't unmarshal data from m.Data")
		return
	}

	// Вызвать метод MoveCourier у courierService
	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)
}
