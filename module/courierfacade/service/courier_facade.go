package service

import (
	"context"
	"fmt"

	cservice "gitlab.com/mezigar/geotask/module/courier/service"
	cfm "gitlab.com/mezigar/geotask/module/courierfacade/models"
	oservice "gitlab.com/mezigar/geotask/module/order/service"
)

const (
	CourierVisibilityRadius = 2800 // 2.8km
)

type CourierFacer interface {
	MoveCourier(ctx context.Context, direction, zoom int) // отвечает за движение курьера по карте direction - направление движения, zoom - уровень зума
	GetStatus(ctx context.Context) cfm.CourierStatus      // отвечает за получение статуса курьера и заказов вокруг него
}

// CourierFacade фасад для курьера и заказов вокруг него (для фронта)
type CourierFacade struct {
	courierService cservice.Courierer
	orderService   oservice.Orderer
}

func NewCourierFacade(courierService cservice.Courierer, orderService oservice.Orderer) CourierFacer {
	return &CourierFacade{courierService: courierService, orderService: orderService}
}

// MoveCourier перемещает курьера по карте в указанном направлении и уровне зума.
func (cf *CourierFacade) MoveCourier(ctx context.Context, direction, zoom int) {
	courier, err := cf.courierService.GetCourier(ctx)
	if err != nil {
		fmt.Println("courier not found in this context")
		return
	}
	err = cf.courierService.MoveCourier(*courier, direction, zoom)
	if err != nil {
		fmt.Println("can't move courier in this direction")
		return
	}
}

// GetStatus возвращает статус курьера и заказов вокруг него.
func (cf *CourierFacade) GetStatus(ctx context.Context) cfm.CourierStatus {
	courier, err := cf.courierService.GetCourier(ctx)
	if err != nil {
		fmt.Println("can't get courier in GetStatus")
		return cfm.CourierStatus{}
	}

	orders, err := cf.orderService.GetByRadius(ctx, courier.Location.Lng, courier.Location.Lat, CourierVisibilityRadius, "km")
	if err != nil {
		fmt.Println("can't get orders in GetStatus")
		return cfm.CourierStatus{}
	}

	return cfm.CourierStatus{
		Courier: *courier,
		Orders:  orders,
	}

}
