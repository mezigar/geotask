package models

import (
	cm "gitlab.com/mezigar/geotask/module/courier/models"
	om "gitlab.com/mezigar/geotask/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
