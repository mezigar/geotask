package storage

import (
	"context"
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/redis/go-redis/v9"
	"gitlab.com/mezigar/geotask/cache"
	"gitlab.com/mezigar/geotask/module/order/models"
)

type OrderStorager interface {
	Save(ctx context.Context, order models.Order, maxAge time.Duration) error                       // сохранить заказ с временем жизни
	GetByID(ctx context.Context, orderID int) (*models.Order, error)                                // получить заказ по id
	GenerateUniqueID(ctx context.Context) (int64, error)                                            // сгенерировать уникальный id
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // получить заказы в радиусе от точки
	GetCount(ctx context.Context) (int, error)                                                      // получить количество заказов
	RemoveOldOrders(ctx context.Context, maxAge time.Duration) error                                // удалить старые заказы по истечению времени maxAge
}

type OrderStorage struct {
	storage cache.RedisClient
}

func NewOrderStorage(storage cache.RedisClient) OrderStorager {
	return &OrderStorage{storage: storage}
}

func (o *OrderStorage) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	err := o.saveOrderWithGeo(ctx, order, maxAge)
	if err != nil {
		return err
	}

	// добавляем ордер в упорядоченное множество для получения количества заказов со сложностью O(1)
	// Score - время создания ордера
	orderKey := fmt.Sprintf("order:%d", order.ID)
	err = o.storage.Client.ZAdd(ctx, "order_scores", redis.Z{
		Score:  float64(order.CreatedAt.Unix()),
		Member: orderKey,
	}).Err()
	if err != nil {
		return err
	}

	return nil
}

func (o *OrderStorage) getOldOrdersWithGeo(ctx context.Context, maxAge time.Duration) ([]string, error) {
	threshold := time.Now().Add(-maxAge).Unix()

	oldOrderKeys, err := o.storage.Client.ZRangeByScore(ctx, "order_scores", &redis.ZRangeBy{
		Min: "-inf",
		Max: fmt.Sprintf("(%d", threshold),
	}).Result()
	if err != nil {
		return nil, err
	}

	return oldOrderKeys, nil
}

func (o *OrderStorage) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {
	// TODO: получить ID всех старых ордеров, которые нужно удалить

	// сохраняем старые ордеры с геоданными перед удалением
	oldOrderKeys, err := o.getOldOrdersWithGeo(ctx, maxAge)
	if err != nil {
		return err
	}

	// Удаляем старые ордеры из Redis
	pipe := o.storage.Client.TxPipeline()

	for _, orderKey := range oldOrderKeys {
		pipe.Del(ctx, orderKey)
		pipe.ZRem(ctx, "orders", orderKey)
		pipe.ZRem(ctx, "order_scores", orderKey)
	}

	_, err = pipe.Exec(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (o *OrderStorage) GetByID(ctx context.Context, orderID int) (*models.Order, error) {
	var err error
	var data []byte
	orderKey := fmt.Sprintf("order:%d", orderID)
	// получаем ордер из redis по ключу order:ID
	data, err = o.storage.Client.Get(ctx, orderKey).Bytes()
	// проверяем что ордер не найден исключение redis.Nil, в этом случае возвращаем nil, nil
	if err == redis.Nil {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	// десериализуем ордер из json
	var order models.Order
	err = json.Unmarshal(data, &order)
	if err != nil {
		return nil, err
	}

	return &order, nil

}

func (o *OrderStorage) saveOrderWithGeo(ctx context.Context, order models.Order, maxAge time.Duration) error {
	var err error
	var data []byte

	// сериализуем ордер в json
	data, err = json.Marshal(order)
	if err != nil {
		return err
	}

	orderKey := fmt.Sprintf("order:%d", order.ID)

	// сохраняем ордер в Redis в формате JSON с заданным временем жизни
	err = o.storage.Client.Set(ctx, orderKey, data, maxAge).Err()
	if err != nil {
		return err
	}

	// добавляем ордер в гео-индекс, используя метод GeoAdd
	err = o.storage.Client.GeoAdd(ctx, "orders", &redis.GeoLocation{
		Name:      orderKey,
		Longitude: order.Lng,
		Latitude:  order.Lat,
	}).Err()
	if err != nil {
		return err
	}

	// добавляем ордер в упорядоченное множество для получения количества заказов со сложностью O(1)
	// Score - время создания ордера
	err = o.storage.Client.ZAdd(ctx, "order_scores", redis.Z{
		Score:  float64(order.CreatedAt.Unix()),
		Member: orderKey,
	}).Err()
	if err != nil {
		return err
	}

	return nil

}

func (o *OrderStorage) GetCount(ctx context.Context) (int, error) {
	// получить количество ордеров в упорядоченном множестве используя метод ZCard
	count, err := o.storage.Client.ZCard(ctx, "orders").Result()
	if err != nil {
		return 0, err
	}
	return int(count), nil
}

func (o *OrderStorage) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	var err error
	var orders []models.Order
	var ordersLocation []redis.GeoLocation

	// используем метод getOrdersByRadius для получения ID заказов в радиусе
	ordersLocation, err = o.getOrdersByRadius(ctx, lng, lat, radius, unit)
	// обратите внимание, что в случае отсутствия заказов в радиусе
	// метод getOrdersByRadius должен вернуть nil, nil (при ошибке redis.Nil)
	if err == redis.Nil {
		return nil, nil
	}
	if err != nil {
		return nil, err
	}
	orders = make([]models.Order, 0, len(ordersLocation))
	// проходим по списку ID заказов и получаем данные о заказе
	for _, orderLocation := range ordersLocation {
		orderID, err := strconv.ParseInt(orderLocation.Name, 10, 64)
		if err != nil {
			// Пропускаем некорректный ID заказа
			continue
		}

		// Получаем данные о заказе из Redis по ключу order:ID
		order, err := o.GetByID(ctx, int(orderID))
		if err != nil {
			// Пропускаем заказ, если произошла ошибка при получении данных
			continue
		}

		orders = append(orders, *order)
	}

	return orders, nil
}

func (o *OrderStorage) getOrdersByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]redis.GeoLocation, error) {
	// в данном методе мы получаем список ордеров в радиусе от точки
	// возвращаем список ордеров с координатами и расстоянием до точки

	geoQuery := &redis.GeoRadiusQuery{
		Radius:      radius,
		Unit:        unit,
		WithCoord:   true,
		WithDist:    true,
		WithGeoHash: true,
	}
	// Выполняем гео-запрос с использованием метода GeoRadius
	orderLocations, err := o.storage.Client.GeoRadius(ctx, "orders", lng, lat, geoQuery).Result()
	if err != nil {
		return nil, err
	}

	return orderLocations, nil

}

func (o *OrderStorage) GenerateUniqueID(ctx context.Context) (int64, error) {
	var err error
	var id int64
	// генерируем уникальный ID для ордера
	// используем для этого redis incr по ключу order:id
	id, err = o.storage.Client.Incr(ctx, "order:id").Result()
	if err != nil {
		return 0, err
	}

	return id, nil
}
