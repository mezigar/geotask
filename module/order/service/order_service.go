package service

import (
	"context"
	"math/rand"
	"time"

	"gitlab.com/mezigar/geotask/geo"
	"gitlab.com/mezigar/geotask/module/order/models"
	"gitlab.com/mezigar/geotask/module/order/storage"
)

const (
	minDeliveryPrice = 100.00
	maxDeliveryPrice = 500.00

	maxOrderPrice = 3000.00
	minOrderPrice = 1000.00

	orderMaxAge = 2 * time.Minute
)

type OrderService struct {
	storage       storage.OrderStorager
	allowedZone   geo.PolygonChecker
	disabledZones []geo.PolygonChecker
}

type Orderer interface {
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error)
	Save(ctx context.Context, order models.Order) error
	GetCount(ctx context.Context) (int, error)
	RemoveOldOrders(ctx context.Context) error
	GenerateOrder(ctx context.Context) error
}

func NewOrderService(storage storage.OrderStorager, allowedZone geo.PolygonChecker, disallowedZone []geo.PolygonChecker) Orderer {
	return &OrderService{storage: storage, allowedZone: allowedZone, disabledZones: disallowedZone}
}

func (s *OrderService) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	return s.storage.GetByRadius(ctx, lng, lat, radius, unit)
}

func (s *OrderService) Save(ctx context.Context, order models.Order) error {
	return s.storage.Save(ctx, order, orderMaxAge)
}

func (s *OrderService) GetCount(ctx context.Context) (int, error) {
	return s.storage.GetCount(ctx)
}

func (s *OrderService) RemoveOldOrders(ctx context.Context) error {
	return s.storage.RemoveOldOrders(ctx, orderMaxAge)
}

func (s *OrderService) GenerateOrder(ctx context.Context) error {
	// Генерация случайной точки в разрешенной зоне
	randomPoint := s.allowedZone.RandomPoint()

	// Генерация уникального ID для заказа
	orderID, err := s.storage.GenerateUniqueID(ctx)
	if err != nil {
		return err
	}

	// Генерация случайной цены для заказа
	orderPrice := generateRandomPrice(minOrderPrice, maxOrderPrice)

	// Генерация случайной цены доставки
	deliveryPrice := generateRandomPrice(minDeliveryPrice, maxDeliveryPrice)

	// Создание нового заказа
	order := models.Order{
		ID:            orderID,
		Price:         orderPrice,
		DeliveryPrice: deliveryPrice,
		Lng:           randomPoint.Lng,
		Lat:           randomPoint.Lat,
		IsDelivered:   false,
		CreatedAt:     time.Now(),
	}

	// Сохранение заказа
	err = s.Save(ctx, order)
	if err != nil {
		return err
	}

	return nil
}

func generateRandomPrice(min, max float64) float64 {
	return min + rand.Float64()*(max-min)
}
