package storage

import (
	"context"
	"encoding/json"

	"gitlab.com/mezigar/geotask/cache"

	"gitlab.com/mezigar/geotask/module/courier/models"
)

type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
	CreateOne(ctx context.Context) (*models.Courier, error)
}

type CourierStorage struct {
	storage cache.RedisClient
}

func NewCourierStorage(storage cache.RedisClient) CourierStorager {
	return &CourierStorage{storage: storage}
}

func (cs *CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	courierJSON, err := json.Marshal(courier)
	if err != nil {
		return err
	}

	err = cs.storage.Client.Set(ctx, "courier", courierJSON, 0).Err()
	if err != nil {
		return err
	}

	return nil
}

func (cs *CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	val, err := cs.storage.Client.Get(ctx, "courier").Result()
	if err != nil {
		return nil, err
	}

	courier := &models.Courier{}
	err = json.Unmarshal([]byte(val), courier)
	if err != nil {
		return nil, err
	}

	return courier, nil
}

func (cs *CourierStorage) CreateOne(ctx context.Context) (*models.Courier, error) {
	courier := models.Courier{
		Location: models.Point{Lat: 55, Lng: 54},
		Score:    1,
	}

	err := cs.Save(ctx, courier)
	if err != nil {
		return nil, err
	}

	return &courier, nil
}
