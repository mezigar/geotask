package service

import (
	"context"
	"math"

	"gitlab.com/mezigar/geotask/geo"
	"gitlab.com/mezigar/geotask/module/courier/models"
	"gitlab.com/mezigar/geotask/module/courier/storage"
)

// Направления движения курьера
const (
	DirectionUp    = 0
	DirectionDown  = 1
	DirectionLeft  = 2
	DirectionRight = 3
)

const (
	DefaultCourierLat = 59.9311
	DefaultCourierLng = 30.3609
)

type Courierer interface {
	GetCourier(ctx context.Context) (*models.Courier, error)
	MoveCourier(courier models.Courier, direction, zoom int) error
}

type CourierService struct {
	courierStorage storage.CourierStorager
	allowedZone    geo.PolygonChecker
	disabledZones  []geo.PolygonChecker
}

func NewCourierService(courierStorage storage.CourierStorager, allowedZone geo.PolygonChecker, disbledZones []geo.PolygonChecker) Courierer {
	return &CourierService{courierStorage: courierStorage, allowedZone: allowedZone, disabledZones: disbledZones}
}

func (c *CourierService) GetCourier(ctx context.Context) (*models.Courier, error) {
	// Получаем курьера из хранилища, используя метод GetOne из storage/courier.go
	courier, err := c.courierStorage.GetOne(ctx)
	if err != nil {
		return nil, err
	}

	if !c.allowedZone.Contains(geo.Point{Lat: courier.Location.Lat, Lng: courier.Location.Lng}) {
		// Курьер находится в запрещенной зоне, перемещаем его в случайную точку внутри разрешенной зоны
		newLocation := geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
		courier.Location = models.Point{Lat: newLocation.Lat, Lng: newLocation.Lng}

		// Сохраняем новые координаты курьера
		err = c.courierStorage.Save(ctx, *courier)
		if err != nil {
			return nil, err
		}
	}

	return courier, nil
}

// MoveCourier : direction - направление движения курьера, zoom - зум карты
func (c *CourierService) MoveCourier(courier models.Courier, direction, zoom int) error {
	// Точность перемещения зависит от зума карты, используем формулу 0.001 / 2^(zoom - 14)
	accuracy := 0.001 / math.Pow(2, float64(zoom-14))

	// Определяем изменение координат в зависимости от направления движения
	switch direction {
	case DirectionUp:
		courier.Location.Lat += accuracy
	case DirectionDown:
		courier.Location.Lat -= accuracy
	case DirectionLeft:
		courier.Location.Lng -= accuracy
	case DirectionRight:
		courier.Location.Lng += accuracy
	}

	// Преобразуем тип courier.Location в geo.Point
	newGeoPoint := geo.Point{
		Lat: courier.Location.Lat,
		Lng: courier.Location.Lng,
	}

	// Проверяем, что курьер не вышел за границы зоны
	if !c.allowedZone.Contains(newGeoPoint) {
		// Если курьер вышел за границы зоны, перемещаем его в случайную точку внутри зоны
		newLocation := geo.GetRandomAllowedLocation(c.allowedZone, c.disabledZones)
		courier.Location = models.Point{
			Lat: newLocation.Lat,
			Lng: newLocation.Lng,
		}
	}

	// Сохраняем изменения в хранилище
	err := c.courierStorage.Save(context.Background(), courier)
	if err != nil {
		return err
	}

	return nil
}
